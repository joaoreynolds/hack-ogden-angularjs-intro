'use strict';

angular.module('projectApp').filter('dashes',function(){
	return function(input){
		return input.replace(/\s/g, '-');
	};
});