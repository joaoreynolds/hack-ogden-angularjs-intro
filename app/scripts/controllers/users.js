'use strict';

angular.module('projectApp').controller('UsersCtrl', function ($scope) {
    
	$scope.newUser = {};

	$scope.users = [
		{
			id: 1,
			name: 'John Reynolds',
			email: 'john@bluepinemedia.com'
		},
		{
			id: 2,
			name: 'Joel Smith',
			email: 'joel@joelsmith.com'
		}
	];

	$scope.addUser = function(){
		$scope.users.push($scope.newUser);
		$scope.newUser = {};
	};

});
