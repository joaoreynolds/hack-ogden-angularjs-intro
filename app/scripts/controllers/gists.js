'use strict';

angular.module('projectApp').controller('GistsCtrl', function ($scope, Gist) {

	$scope.gists = Gist.query();

	$scope.myGist = Gist.get({id: 'a95f2e103183e058bb9d'});

});
