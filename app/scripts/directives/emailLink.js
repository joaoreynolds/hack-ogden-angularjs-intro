'use strict';

angular.module('projectApp').directive('emailLink',function (){
	return {
		restrict: 'A',
		template: '<a href="mailto:{{email}}">{{email}}</a>',
		scope: {
			email: '=emailLink'
		},
		link: function (scope) {

			//manipulate the data here
			console.log(scope.email);

		}
	};
});