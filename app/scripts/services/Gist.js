'use strict';

angular.module('projectApp').factory('Gist',function ($resource){
    return $resource('https://api.github.com/gists/:id');
});