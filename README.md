# Hack Ogden AngularJS Intro #

These are the files used and created in the presentation given at Hack Ogden by John Reynolds.

These files were bootstrapped using the scaffolding tool Yeoman Angular.

For questions or comments email John Reynolds: john [at] bluepinemedia.com

### Getting the files ###

Get files by running `git clone https://joaoreynolds@bitbucket.org/joaoreynolds/hack-ogden-angularjs-intro.git`

There are several tags which mark certain steps in the presentation, you can go back to any by running `git checkout -f step-[step_number]` from inside the project's folder `hack-ogden-angularjs-intro`. For example: `git checkout -f step-0`.

### Development Environment ###

The Yeoman project comes setup with several dependencies, including:

* Sass and Compass (both of which run on Ruby)
* NodeJS
* Bower

After installing dependencies, you can run: `node install` and then `bower install` to download node dependencies and bower dependencies.

To preview the project in a browser using a node server, run `grunt serve`.

I know this is overkill for the presentation's sake, but this is how I'm used to setting up a development environment, and thanks to Yeoman, was the quickest way to get a project up and running, even if it's a small one.